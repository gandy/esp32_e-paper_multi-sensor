# DIY multi sensor with e-paper display

## Hardware
1. [ESP32 Dev Board](https://www.espressif.com/en/products/devkits/esp32-devkitc/overview)
2. [DHT22 - temperature and humidity sensor](https://www.waveshare.com/wiki/DHT22_Temperature-Humidity_Sensor)
3. [e-Paper display - waveshare 1.54inch](https://www.waveshare.com/wiki/1.54inch_e-Paper_Module)
4. [SGP30 TVOC/eCO2 Gas Sensor](https://learn.adafruit.com/adafruit-sgp30-gas-tvoc-eco2-mox-sensor/arduino-code)

## Development environment
1. install Arduino IDE using flatpak

    $ flatpak search arduino

2. add additional URL for espressif boars
> Add to: File -> Preferences -> Addotional Boards Manager URLs:

    https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json, http://arduino.esp8266.com/stable/package_esp8266com_index.json

3. add esp32 board
- start *Boards Manager*
> Tools -> Board: "XXX Module" -> Boards Manager ...
- search for *ESP32* and install
- choose *ESP32 Dev Module*

4. add libraries
- start *Manage Libraries*
> Sketch -> Include Library -> Manage Libraries ...
- search and install - *EPD* (by Asuki Kono), *DHT sensor library* (by Adafruit with dependencies), *PubSubClient* (by Nick O'Leary)

## Verify, build and upload
1. rename *secret.example* to *secret.c* and edit WIFI SSID and password
2. verify
> Sketch -> Verify/Compile
3. upload - after compile boot button on board have to be pushed
> Sketch -> Upload
